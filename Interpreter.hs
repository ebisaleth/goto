module Interpreter where 

-- TODO: Make the program counter the result of a computation (it is, anyway.)

import Grammar
import Control.Applicative hiding (empty)
import Control.Monad
import Prelude hiding (lookup)
import Data.Map.Lazy 
import Data.Tuple


------------------------------------------------------
-- I built a state monad with my bare hands again   --
-- because I really like to see what I'm doing.     --
------------------------------------------------------

type VariableStates = Map String Integer 

type State = (Integer, VariableStates)

data Comp result = Comp {runcomp :: (State -> (result, State))} 

instance Functor Comp where
  fmap = liftM

instance Applicative Comp where
  pure = return 
  (<*>)= ap

instance Monad Comp where 
  return thingy = Comp $ \s -> (thingy,s) -- don't touch the state
  (>>=) (Comp comp) mf = Comp $ \olds -> 
    let (res,news) = comp olds       
    in runcomp (mf res) news
    
    
------------------------------------------------------
-- primitive state operations                       --
------------------------------------------------------


updatevar :: Variable -> Integer -> Comp ()
updatevar v x = Comp $ \(pc, env) -> ((),(pc, insert v x env))

findvar :: Variable -> Comp Integer
findvar v = Comp $ \(pc, env) -> (findWithDefault 0 v env, (pc, env))


getpc :: Comp Integer 
getpc = Comp $ \(pc,env) -> (pc,(pc,env))

setpc :: Integer -> Comp ()
setpc mn = Comp $ \(_,env) -> ((),(mn,env))

incpc :: Comp ()
incpc = Comp $ \(pc, env) -> ((),(pc+1, env))
 
 
------------------------------------------------------
-- operations corresponding to the types of GOTO    --
-- lines                                            --
------------------------------------------------------ 

add :: Variable -> Variable -> Constant -> Comp () 
-- x1 := x2 + c
add v1 v2 c = do x2 <- findvar v2 
                 updatevar v1 (x2 + c)
                 incpc 
                 
   
sub :: Variable -> Variable -> Constant -> Comp () 
-- x1 := x2 + c
sub v1 v2 c = do x2 <- findvar v2 
                 updatevar v1 (x2 -. c)
                 incpc
                   where                 
                    (-.) :: Integer -> Integer -> Integer 
                    (-.) x y | x > y     = x - y
                             | otherwise = 0
                 

goto :: Integer -> Comp () 
goto n = setpc n 

ifgoto :: Variable -> Constant -> Integer -> Comp () 
ifgoto v c n = do x <- findvar v
                  if (x == c) then goto n else incpc
halt :: Comp ()
halt = setpc (-1) 

------------------------------------------------------
-- running a goto program                           --
------------------------------------------------------ 

rungoto :: State -> Prog -> VariableStates 
rungoto s p = snd $ snd $ runcomp (execute (goto 0) s p) s

execute :: Comp () -> State -> Prog -> Comp ()  
execute c s prog = do let (_,(pc,v)) = (runcomp c) s
                      case lookup pc prog of 
                        Nothing -> c 
                        Just l  -> do c 
                                      execute (interpret l) (pc,v) prog

interpret :: Line -> Comp ()
interpret (Add v1 v2 c)  = add v1 v2 c
interpret (Sub v1 v2 c)  = sub v1 v2 c
interpret (GOTO nr)      = goto nr
interpret (IF v c nr)    = ifgoto v c nr
interpret HALT           = halt 

------------------------------------------------------
-- Some examples to see how it works:               --
------------------------------------------------------ 

-- to run a program: 
-- >rungoto examplestate exampleprog

-- to start execution from a certain line: 
-- >execute (goto 2) examplestate exampleprog

-- run a single line: 
-- runcomp (goto 2) examplestate

examplestate = (0,empty)
exampleprog  = fromList $ zip [0..] 
                              [(Add "a" "a" 1),
                               (IF "a" 5 3),
                               (GOTO 0),
                               (HALT)] 

