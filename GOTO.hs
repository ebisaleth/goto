module Main where 

import System.Environment
import System.IO

import Parser 
import Interpreter
import Data.Map (fromList, findWithDefault)

main :: IO ()
main = do
    (filename:args) <- getArgs 
    let intargs = fmap read args :: [Integer]
    let state = zip (fmap varname [1..]) intargs 
    handle <- openFile filename ReadMode
    input  <- hGetContents handle
    let parsed = parse input 
    let res = findWithDefault 0 "x0" $ rungoto (0, fromList state) $ fromList parsed 
    putStrLn $ show res
    hClose handle
    
varname :: Integer -> String
varname i = 'x':(show i)
