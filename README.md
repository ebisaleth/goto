Hello :sunny: 

This is a simple [GOTO](https://de.wikipedia.org/wiki/GOTO-Programm) Interpreter 


# to use this

* the file named GOTO is the binary 
* you can just download it and run it from a terminal
* for example, to compute 2 * 2, run 
* you might need to add the permission first 

`./GOTO times.txt 2 2`

# To compile this you will need 

* GHC 
* this baby here https://hackage.haskell.org/package/split
* and this one https://hackage.haskell.org/package/regex-tdfa


# Syntax speccccs 

* variables start with a lowercase letter and can use uppercase letters, lowercase letters, digits, and '_'. (`x1`, `x_2`, `thisIsAnAmazingVariableName9992` are all valid choices.)
* labels start with an uppercase letter and can use uppercase letters, lowercase letters, digits, and '_'. (`M1`, `M_emes`, `HelpMeIamStuckInsideAGOTOProgram` are all excellent choices.)
* you don't need to put a colon after your labels. we don't care. (if you want to be mean to your students for 'pedagogical reasons' and force them to do it correctly, comment lines 57-58 of `Parser.hs`)
* you can use "==" and "=" interchangably (if you want to be mean to your students for 'pedagogical reasons', comment line 51 of `Parser.hs`)


# Additional information for writing your own GOTO program: 

* The arguments will be placed in `x1,  x2 ... xn`
* At the end, the content of `x0` will be printed. 
* So maybe put the result of your computation in there. Or don't. I'm not telling you what to do. 
* ~~The tokenizer is a little baby robot :robot:, and it doesn't yet understand tokens that aren't separated by whitespace. So instead of`x:=x+1` you'll have to space it out and write `x := x + 1`.~~ that's not true anymore!! the tokenizer has grown up, it can understand `x:=x+1` AND YOU SHOULD FEAR IT :robot: :fire: 
