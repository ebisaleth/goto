{-# LANGUAGE ViewPatterns #-}

module Parser where 


import Data.List hiding (insert, lookup)
import Text.Regex.TDFA hiding (empty)
import Text.Regex.TDFA.Text ()
import Grammar 
import Prelude hiding (lookup)
import Data.Char (isSpace)
import Data.List.Split (splitOneOf)
import Data.Map.Lazy hiding (filter, null)


parse = fmap parseline . labelhack . tokenize . clean 

----------------------------------------------------------------
-- STEP ONE:                                                  --
-- clean up the input (remove comments, lines it, words it.)  --
-- you can use ;, linebreaks, or both.                        --
----------------------------------------------------------------

clean = fmap words . fmap (takeWhile (`notElem` "#")) . filter (not . \x -> head x == '#') . filter (not.null) . fmap trim . splitOneOf ";\n"

trim :: String -> String
trim = f . f
   where f = reverse . dropWhile isSpace

-----------------------------------------------------------------
-- STEP TWO:                                                   --
-- tokenizeing. _very_ dirty.                                  --
-----------------------------------------------------------------
        

tokenize :: [[String]] -> [[Token]]
tokenize =  fmap $ concatMap $ tokenify 
   
data Token = TVar String | TLabel String | TNr Integer | TConst Integer | TIF | TGOTO | TTHEN | THALT | TAssign | TMinus | TPlus | TEq deriving Show


tokenify :: String -> [Token]
tokenify [] = []
tokenify (stripPrefix "GOTO" -> Just rest) = TGOTO : (tokenify rest)
tokenify (stripPrefix "IF" -> Just rest)   = TIF : (tokenify rest) 
tokenify (stripPrefix "HALT" -> Just rest) = THALT : (tokenify rest)
tokenify (stripPrefix "THEN" -> Just rest) = TTHEN : (tokenify rest)
tokenify (stripPrefix "+" -> Just rest)    = TPlus : (tokenify rest)
tokenify (stripPrefix "-" -> Just rest)    = TMinus : (tokenify rest)
tokenify (stripPrefix "=" -> Just rest)    = TEq : (tokenify rest)
tokenify (stripPrefix "==" -> Just rest)   = TEq : (tokenify rest) -- bonus, not in the spec. makes == usable instead of =.
tokenify (stripPrefix ":=" -> Just rest)   = TAssign : (tokenify rest)
tokenify (flip (=~) "[a-z]([A-Z]|[a-z]|[0-9]|\\_)*" -> ("", var, rest))        
                                                        = (TVar var) : (tokenify rest)
tokenify (flip (=~) "[A-Z]([A-Z]|[a-z]|[0-9]|\\_)*\\:" -> ("", label, rest)) 
                                                        = (TLabel (init label)) : (tokenify rest)
tokenify (flip (=~) "[A-Z]([A-Z]|[a-z]|[0-9]|\\_)*" -> ("", label, rest)) -- bonus, not in the spec. makes : after labels optional. 
                                                        = (TLabel label) : (tokenify rest)
tokenify (flip (=~) "[0-9]+" -> ("", const, rest))      = case (reads const :: [(Integer,String)]) of 
                                             [(i,"")]  -> (TConst i) : (tokenify rest)
                                             _         -> error $ "for some reason i thought i could make : \"" ++ const ++ "\" into a number, but alas, i could not"
tokenify s                                              = error $ "sorry i can't parse this thing: \"" ++ s ++ "\""


-----------------------------------------------------------------
-- STEP THREE:                                                 --
--- stupid label hacking... please don't look I'm ashamed      --
-----------------------------------------------------------------
-- TODO: * reflect the distinction from pre- to postproduced labels on typelevel 



labelhack :: [[Token]] -> [[Token]]
labelhack ts = fmap (replacelabels (labelmap ts)) ts

labelmap :: [[Token]] -> Map String Integer 
labelmap toks = lm toks empty 0 where 
  lm :: [[Token]] -> Map String Integer -> Integer -> Map String Integer 
  lm []                m _ = m
  lm (((TLabel str):_):ts) m i = lm ts (insert str i m) (i+1)
  lm (( _          :_):ts) m i = lm ts m                 i
  
replacelabels :: Map String Integer -> [Token] -> [Token]
replacelabels _ []                = []
replacelabels m ((TLabel str):ts) = (TNr (lookupOrThrowUp str m)):replacelabels m ts
replacelabels m (           t:ts) = t:(replacelabels m ts)


lookupOrThrowUp :: (Ord k, Show k, Show a) => k -> Map k a -> a 
lookupOrThrowUp k m = case lookup k m of 
                        Just v -> v
                        _      -> error $ "There is no key" ++ (show k) ++ " in Map " ++ (show m) ++ "."
                        
                        
-----------------------------------------------------------------
-- STEP FOUR:                                                  --
--- parsing whole lines                                        --
-----------------------------------------------------------------
-- TODO: * allow lines that don't have a label (should go to step 3 as well)

parseline :: [Token] -> (Integer,Line)
parseline [(TNr nr),(TVar x1),(TAssign),(TVar x2),(TPlus),(TConst c)] = (nr, Add x1 x2 c)
parseline [(TNr nr),(TVar x1),(TAssign),(TVar x2),(TMinus),(TConst c)] = (nr, Sub x1 x2 c)
parseline [(TNr nr),(TGOTO),(TNr label)] = (nr, (GOTO label))
parseline [(TNr nr),(TIF),(TVar x),(TEq),(TConst c),(TTHEN),(TGOTO),(TNr label)] = (nr, (IF x c label))
parseline [(TNr nr),(THALT)] = (nr, (HALT))
parseline line = error (show line)




















  
