       
-- <Goto-Programm> := M_k : x_i := x_j + c 
--                    | M k : x_i := x_j – c
--                    |<Goto-Programm>;<Goto-Programm>
--                    | M_k : GOTO M_j
--                    | M_k : IF x i = c THEN GOTO M_j
--                    | M_k : HALT      
       
       
module Grammar where        
       
import Data.Map.Lazy hiding (filter)
       
type Variable        = String 
type Label           = Integer
type Constant        = Integer 
data Line            = Add Variable Variable Constant | Sub Variable Variable Constant | GOTO Label | IF Variable Constant Label | HALT deriving Show 


type Prog     = (Map Label Line) 




